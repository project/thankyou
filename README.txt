-- SUMMARY --

This module creates a configurable Thank You or Post Submit page to redirect
your users after they have created a new node.

When you create a new node in Drupal, the default behavior is to show the newly
created node with a highlighted box indicating that the node was created.

A good use case for this module is when a newly created node is set to
"unpublished" by default. In this scenario your user may not see the new node if
he/she does not have access to view unpublished content. To provide a better
user experience you may want to redirect the user to a page confirming that the
node has been created.

What this module can do can be also accomplished by using the module Rules;
however if all you want to do is to provide a simple Thank You page and you
don't want to install a larger module or you don't need all the other
functionalities provided by Rules then you can use this module.

-- INSTALLATION --

Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

Configure at: [Your Site]/admin/config/content/thankyou
...or: Configuration > Content Authoring > ThankYou Settings

-- CONTACT --

Current maintainers:
* Juan Carlos Martinez (jcmartinez) - http://drupal.org/user/275152

This project has been sponsored by:
* OGILVY & MATHER